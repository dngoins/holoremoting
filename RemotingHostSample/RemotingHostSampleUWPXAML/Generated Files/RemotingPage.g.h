﻿#pragma once
//------------------------------------------------------------------------------
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
//------------------------------------------------------------------------------


namespace Windows {
    namespace UI {
        namespace Xaml {
            namespace Controls {
                ref class SwapChainPanel;
                ref class TextBox;
                ref class Button;
                ref class TextBlock;
            }
        }
    }
}

namespace RemotingHostSample
{
    [::Windows::Foundation::Metadata::WebHostHidden]
    partial ref class RemotingPage : public ::Windows::UI::Xaml::Controls::Page, 
        public ::Windows::UI::Xaml::Markup::IComponentConnector,
        public ::Windows::UI::Xaml::Markup::IComponentConnector2
    {
    public:
        void InitializeComponent();
        virtual void Connect(int connectionId, ::Platform::Object^ target);
        virtual ::Windows::UI::Xaml::Markup::IComponentConnector^ GetBindingConnector(int connectionId, ::Platform::Object^ target);
    
    private:
        bool _contentLoaded;
    
        private: ::Windows::UI::Xaml::Controls::SwapChainPanel^ swapChainPanel;
        private: ::Windows::UI::Xaml::Controls::TextBox^ ipAddress;
        private: ::Windows::UI::Xaml::Controls::Button^ Start;
        private: ::Windows::UI::Xaml::Controls::Button^ Stop;
        private: ::Windows::UI::Xaml::Controls::Button^ TogglePreview;
        private: ::Windows::UI::Xaml::Controls::TextBox^ tbModelFileName;
        private: ::Windows::UI::Xaml::Controls::Button^ modelPicker;
        private: ::Windows::UI::Xaml::Controls::TextBlock^ Console;
    };
}

